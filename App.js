import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import SigninPatientScreen from './src/screens/SigninPatientScreen';
import SigninCareTeamScreen from './src/screens/SigninCareTeamScreen';
import PatientSelectionScreen from './src/screens/PatientSelectionScreen';
import PatientReportScreen from './src/screens/PatientReportScreen';
import SignalsScreen from './src/screens/SignalsScreen';
import BarcodeScannerScreen from './src/screens/BarcodeScannerScreen';

import SignalDetailsScreen from './src/screens/SignalDetailsScreen';
import RegistrationFormScreen from './src/screens/RegistrationFormScreen';

import { Provider } from "react-redux";
import store from "./src/state/configure.store";

const LoginStack = createStackNavigator();
function LoginStackScreen() {
  return (
    <LoginStack.Navigator>
      <LoginStack.Screen name="SigninPatient" component={SigninPatientScreen} options={{ headerShown: false }}/>
      <LoginStack.Screen name="SigninCareTeam" component={SigninCareTeamScreen} options={{ headerShown: false }}/>
      <LoginStack.Screen name="PatientSelection" component={PatientSelectionScreen} options={{ headerShown: false }}/>
      <LoginStack.Screen name="BarcodeScanner" component={BarcodeScannerScreen} options={{ headerShown: false }}/>
    </LoginStack.Navigator>
  );
}

const HomeTab = createBottomTabNavigator();
function HomeTabScreen() {
  return (
    <HomeTab.Navigator
      initialRouteName="Signals"
      tabBarOptions={{
        activeTintColor: '#235A9F',
      }}
      >
      <HomeTab.Screen 
        name="Signals" 
        component={SignalsScreen} 
        options={{
          tabBarLabel: 'Signals',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="clipboard-pulse-outline" color={color} size={size} />
          ),
        }}/>
      <HomeTab.Screen 
        name="PatientReport" 
        component={PatientReportScreen} 
        options={{
          tabBarLabel: 'Report',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account-details-outline" color={color} size={size} />
          ),
        }}/>
      <HomeTab.Screen 
        name="RegistrationForm" 
        component={RegistrationFormScreen} 
        options={{
          tabBarLabel: 'Form',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account-plus-outline" color={color} size={size} />
          ),
        }}/>
    </HomeTab.Navigator>
  );
}

const RootStack = createStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <SafeAreaProvider>
        <NavigationContainer>
          <RootStack.Navigator>
            <RootStack.Screen name="Login" component={LoginStackScreen} options={{ headerShown: false }}/>
            <RootStack.Screen name="Home" component={HomeTabScreen} options={{ headerShown: false }} />
          </RootStack.Navigator>
        </NavigationContainer>
      </SafeAreaProvider>
    </Provider>
  );
}

export default App;
  
