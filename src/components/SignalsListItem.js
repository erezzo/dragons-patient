import React from 'react';

import { ListItem, Avatar, Icon,Button} from 'react-native-elements';

import {TouchableOpacity, StyleSheet, ScrollView} from 'react-native'

const SignalListItem = ({ Abbreviation, SignalTime, ValidatedByUser, ParameterId, HasWarning, IsError, ValueNumeric }) => {


    const heartRateImage =require('../assets/images/avater-heart-rate.png');
    const bloodImage =require('../assets/images/avatar-regular-signal.png');


    return <ListItem bottomDivider >
            <Avatar size="small" source={Abbreviation.toLowerCase().includes('heart') ? heartRateImage : bloodImage} />
            <Button titleStyle={IsError ? styles.buttonRedStyle : styles.buttonRedgularStyle} type="outline" title= "Full Info" onPress={() => {
                    alert(`${Abbreviation} Value: ${ValueNumeric.Value} ${ValueNumeric.Unit.Code} `);
                }} />   
            <ListItem.Content>
                <ListItem.Title style={IsError ? styles.redStyle : styles.regularStyle}>{Abbreviation} | Parameter ID: {ParameterId}</ListItem.Title>
                <ListItem.Subtitle>Signal Time: {SignalTime}</ListItem.Subtitle>
                <ListItem.Subtitle>Validated By User: {ValidatedByUser}</ListItem.Subtitle>
                <ListItem.Subtitle>Has Warning: {HasWarning ? 'Yes' : 'No'}</ListItem.Subtitle>                
            </ListItem.Content>
            <ListItem.Chevron />
    </ListItem>
};

const styles = StyleSheet.create({
    redStyle: {
      color: 'red'
    },
    regularStyle: {
        color: 'black'
      },
      buttonRedStyle:{
        color: 'red'
      },
      buttonRegularStyle: {

      }
  });

export default SignalListItem;