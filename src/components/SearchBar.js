import React from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import { Input, Button } from 'react-native-elements';
import { Feather, MaterialCommunityIcons } from '@expo/vector-icons';

const SearchBar = ({ term, onTermChange, onTermSubmit, placeholder, navigation }) => {
  return (
    <View style={styles.backgroundStyle}>
      
      <Input
        autoCapitalize="none"
        autoCorrect={false}
        style={styles.inputStyle}
        placeholder= {placeholder || "Search"}
        value={term}
        onChangeText={onTermChange}
        onEndEditing={onTermSubmit}
        leftIcon={
          <Feather name="search" style={styles.iconStyle} />          
        }
      />
      <Button style={styles.buttonStyle}
        icon={
          <MaterialCommunityIcons name="barcode-scan" size={36} color="black" />}
        onPress={() => navigation.navigate('BarcodeScanner')}
        type="clear"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  backgroundStyle: {
    marginVertical: 10,
    backgroundColor: '#F0EEEE',
    height: 50,
    borderRadius: 10,
    marginHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  inputStyle: {
    flex: 1,
    fontSize: 18,
    paddingLeft: 15
  },
  buttonStyle: {
    alignItems: 'flex-end',
    fontSize: 18,
    paddingRight: 20  
  },
  iconStyle: {
    fontSize: 30,
    alignSelf: 'center',
    marginHorizontal: 15
  }
});

export default SearchBar;