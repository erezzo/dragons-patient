import React from 'react';
import { ListItem, Avatar } from 'react-native-elements';

const PatientListItem = ({ PatientID, FirstName, LastName, MRNumber, Gender }) => {

    const maleImage = require('../assets/images/avatar_patient_boy.png');
    const femaleImage = require('../assets/images/avatar-patient-girl.png');
    const imageSource = Gender.toLowerCase() === 'male' ? maleImage : 
        Gender.toLowerCase() === 'female' ?  femaleImage : 
        PatientID % 2 === 0 ? maleImage : femaleImage;       
    
    return <ListItem bottomDivider>
        <Avatar source={imageSource} />
        <ListItem.Content>
            <ListItem.Title>{FirstName} {LastName}</ListItem.Title>
            <ListItem.Subtitle>MRNumber: {MRNumber}</ListItem.Subtitle>
        </ListItem.Content>
        <ListItem.Chevron />
    </ListItem>
};

export default PatientListItem;