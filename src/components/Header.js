import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Text } from 'react-native-elements';
import { color } from 'react-native-reanimated';

const Header = ({ headerText }) => {
  return <View style={styles.view}>
      <Text style={styles.text}>{headerText}</Text>
      </View>;
};

const styles = StyleSheet.create({
  view: {
    backgroundColor: '#F8F8F8',
    alignItems: 'center',
    justifyContent: 'center', 
    height: 60,
    paddingVertical: 15,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.2,
    elevation: 2,
    position: 'relative'
  },
  text: {
    fontSize: 24,
    color: '#235A9F'
  }
});

export default Header;