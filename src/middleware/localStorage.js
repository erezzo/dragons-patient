import AsyncStorage from '@react-native-async-storage/async-storage';


const getData = async (key='state_data') => {
  try {
    const jsonValue = await AsyncStorage.getItem(key)    
    if (jsonValue === null) {
      return undefined
    }
    JSON.parse(jsonValue)
  } catch(e) {
    return undefined
  }
}

const storeData = async (value, key='state_data') => {
  try {
    if (!value){
      return
    }

    const jsonValue = JSON.stringify(value)
    await AsyncStorage.setItem(key, jsonValue)
  } catch (e) {
    // saving error
  }
};

export default {
  getData,
  storeData
};

  