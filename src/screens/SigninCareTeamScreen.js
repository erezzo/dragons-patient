import React, { useState } from 'react';
import { View, StyleSheet, ActivityIndicator, Image } from 'react-native';
import { Input, Text, Button } from 'react-native-elements';
import { SafeAreaView } from 'react-native-safe-area-context';

import NavLink from '../components/NavLink';
import Spacer from '../components/Spacer';
import Header from '../components/Header';

const SigninCareTeamScreen = ( { navigation} ) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  
  return (
    <SafeAreaView>
      <Header headerText='Care Team'/>
      <View style={styles.container}>      
        <View style={styles.imageConatinerStyle}>
          <Image
            source={require('../assets/images/healthcare.png')}
            resizeMode= 'center'
            style={styles.imageStyle}
            PlaceholderContent={<ActivityIndicator />}
          />               
        </View>   
                   
        <View style={styles.mainConatinerStyle}>       
          <Spacer />        
          <Spacer />  
          <Input
            label="Email"
            value={email}
            onChangeText={setEmail}
            autoCapitalize="none"
            autoCorrect={false}
          />
          <Spacer />        
          <Input
            secureTextEntry
            label="Password"
            value={password}
            onChangeText={setPassword}
            autoCapitalize="none"
            autoCorrect={false}
          />        
          {errorMessage ? (
            <Text style={styles.errorMessage}>{errorMessage}</Text>
          ) : null}
          <Spacer>
            <Button
              title="Sign In"
              onPress={() => navigation.navigate('PatientSelection')}
            />
          </Spacer>
          <NavLink
            navigation={navigation}
            text="Not a Nurse/ Doctor? Sign in as Patient"
            routeName="SigninPatient"
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start'
  },
  imageStyle: {    
    flex: 1,
    height: 200
  },
  mainConatinerStyle: {

  },
  imageConatinerStyle: {    
    height: 200,
    flexDirection: 'row',
    marginTop: 4

  },
  errorMessage: {
    fontSize: 16,
    color: 'red',
    marginLeft: 15,
    marginTop: 15
  }
});

export default SigninCareTeamScreen;
