import React, { useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, FlatList } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useSelector, useDispatch } from "react-redux";
import { getPatients, setPatient } from "../state/actions/patients.actions";
import {  Button, Text } from 'react-native-elements';
import PatientListItem from '../components/PatientListItem';
import Spinner from '../components/Spinner';
import SearchBar from '../components/SearchBar';

const PatientSelectionScreen = ( { navigation } ) => {

  const dispatch = useDispatch();
  const { is_loading_patients, loading_patients_error, patients_list} = useSelector(state => state.patients);
  const [searchTerm, setSearchTerm] = useState('');
  const { barcodeData} = useSelector(state => state.general);

  useEffect(() => {
    dispatch(getPatients());
    setSearchTerm(barcodeData)
  }, [barcodeData]);

  const patientClick = (patient) => {
    console.log(`---Patient[${patient.PatientID}] clicked`)
    dispatch(setPatient(patient));
    navigation.navigate('Home');
  }

  if (is_loading_patients) return (
    <SafeAreaView style={styles.centerStyle}>
      <Spinner style={styles.centerStyle} spinnerText='Loading Patients...'/>      
    </SafeAreaView>
  );

  if (loading_patients_error) return (
    <SafeAreaView style={styles.centerStyle}>
      <Text h3>{loading_patients_error}</Text>
      <Button
            title="Try Again"
            onPress={() => dispatch(getPatients())}
        />          
    </SafeAreaView>
  );
  
 const filtered_list = searchTerm ? patients_list.filter(item =>
    item.FirstName.toLowerCase().includes(searchTerm.toLowerCase()) ||
    item.LastName.toLowerCase().includes(searchTerm.toLowerCase()) ||
    item.MRNumber.toLowerCase().includes(searchTerm.toLowerCase())
  ) : patients_list;

  console.log(`---list count: ${filtered_list.length}`);

  return (
    <SafeAreaView >
      <SearchBar
        term={searchTerm}
        onTermChange={newTerm => setSearchTerm(newTerm)}
        placeholder="Patient Search"
        navigation={navigation}
      />
     
      <FlatList
       data={filtered_list}
       keyExtractor={(result) => result.PatientID.toString()}
       renderItem={({item}) => (
        <TouchableOpacity onPress= {()=> patientClick(item)}>
          <PatientListItem {...item} />
        </TouchableOpacity>
      )}       
       />      
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  centerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
}
});

export default PatientSelectionScreen;
