import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import { useDispatch } from "react-redux";
import { BarCodeScanner } from 'expo-barcode-scanner';
import { setBarcode } from "../state/actions/general.actions";


const BarcodeScannerScreen = ( { navigation } ) => {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const dispatch = useDispatch();
  
  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  const handleBarCodeScanned = ({ type, data }) => {    
    console.log(`Bar code with type ${type} and data ${data} has been scanned!`);
    setScanned(true);
    dispatch(setBarcode(type, data));
    navigation.goBack();
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <View style={styles.container}>
      <BarCodeScanner
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />
      {!scanned && <Button title={'Cancel Scan'} onPress={() => { 
        setScanned(false)
        navigation.goBack();
      }} />}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  
});

export default BarcodeScannerScreen;