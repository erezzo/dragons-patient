import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useSelector, useDispatch } from "react-redux";
import Header from '../components/Header';
import Spacer from '../components/Spacer';

const PatientReportScreen = () => {
  const dispatch = useDispatch();
  const {current_patient} = useSelector(state => state.patients);
 
console.log(`---Patient Report`);
return (
  <SafeAreaView>
    <Header headerText='Patient Report'/>
    <Spacer/>
  <Text style={{textAlign: "center", fontSize: 16}}>
      Patient ID
  </Text> 
  <Text style={{textAlign: "center", fontSize: 24, color: "blue", fontWeight: 'bold'}}>
      {current_patient.PatientID}
  </Text>
  <Text style={{textAlign: "center", fontSize: 16}}>
      First Name
  </Text> 
  <Text style={{textAlign: "center", fontSize: 24, color: 'blue', fontWeight: 'bold'}}>
      {current_patient.FirstName}
  </Text>
  <Text style={{textAlign: "center", fontSize: 16}}>
      Last Name
  </Text> 
  <Text style={{textAlign: "center", fontSize: 24, color: 'blue', fontWeight: 'bold'}}>
      {current_patient.LastName}
  </Text>
  <Text style={{textAlign: "center", fontSize: 16}}>
    MR Number
  </Text> 
  <Text style={{textAlign: "center", fontSize: 24, color: 'blue', fontWeight: 'bold'}}>
      {current_patient.MRNumber}
  </Text>
  <Text style={{textAlign: "center", fontSize: 16}}>
    Account Number
  </Text> 
  <Text style={{textAlign: "center", fontSize: 24, color: 'blue', fontWeight: 'bold'}}>
      {current_patient.AccountNumber}
  </Text>
  <Text style={{textAlign: "center", fontSize: 16}}>
    Admission Date
  </Text> 
  <Text style={{textAlign: "center", fontSize: 24, color: 'blue', fontWeight: 'bold'}}>
      {current_patient.AdmissionDate}
  </Text>
  <Text style={{textAlign: "center", fontSize: 16}}>
    Status
  </Text> 
  <Text style={{textAlign: "center", fontSize: 24, color: 'blue', fontWeight: 'bold'}}>
      {current_patient.Status}
  </Text>
  <Text style={{textAlign: "center", fontSize: 16}}>
  Comment
  </Text> 
  <Text style={{textAlign: "center", fontSize: 24, color: 'blue', fontWeight: 'bold'}}>
      {current_patient.Comment}
  </Text>
  </SafeAreaView>
);
};
const styles = StyleSheet.create({
  DescriptionStyle: {
    textAlign: "center",
    color: "black",
    fontWeight: "normal"
}
});
 
export default PatientReportScreen;