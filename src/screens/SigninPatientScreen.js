import React, { useState } from 'react';
import { View, StyleSheet, ActivityIndicator, Image } from 'react-native';
import { Input, Text, Button } from 'react-native-elements';
import { SafeAreaView } from 'react-native-safe-area-context';

import NavLink from '../components/NavLink';
import Spacer from '../components/Spacer';
import Header from '../components/Header';

const SigninPatientScreen = ({ navigation }) => {
  const [MRNumber, setMRNumber] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  
  return (
    <SafeAreaView>
      <Header headerText='Patient'/>
      <View style={styles.container}>        
        <View style={styles.imageConatinerStyle}>                         
          <Image
            source={require('../assets/images/patient.jpg')}
            resizeMode= 'stretch'
            style={styles.imageStyle}
            PlaceholderContent={<ActivityIndicator />}
          />
        </View>
        <View style={styles.mainConatinerStyle}>   
          <Spacer/>
          <Spacer/>
          <Input
            label="MR Number"
            value={MRNumber}
            onChangeText={setMRNumber}
            autoCapitalize="none"
            autoCorrect={false}
          />
          <Spacer />
          <Input
            secureTextEntry
            label="Password"
            value={password}
            onChangeText={setPassword}
            autoCapitalize="none"
            autoCorrect={false}
          />
          {errorMessage ? (
            <Text style={styles.errorMessage}>{errorMessage}</Text>
          ) : null}
          <Spacer>
            <Button
              title="Sign In"
              onPress={() => navigation.navigate('Home')}
            />
          </Spacer>
          <NavLink
            navigation={navigation}
            text="Not a Patient? Sign in as Care Team"
            routeName="SigninCareTeam"
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start'
  },
  imageStyle: {    
    flex: 1,
    height: 200
  },
  mainConatinerStyle: {

  },
  imageConatinerStyle: {    
    height: 200,
    flexDirection: 'row',
    marginTop: 4

  },
  errorMessage: {
    fontSize: 16,
    color: 'red',
    marginLeft: 15,
    marginTop: 15
  }
});

export default SigninPatientScreen;
