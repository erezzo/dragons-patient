import React, {useEffect} from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import {Button, SearchBar, Text} from 'react-native-elements';
import { getPatientSignals } from '../state/actions/signals.actions';
import { useSelector, useDispatch } from "react-redux";
import { SafeAreaView } from 'react-native-safe-area-context';
import SignalListItem from '../components/SignalsListItem';
import Icon from 'react-native-vector-icons/FontAwesome';

import Header from '../components/Header';
import Spacer from '../components/Spacer';
import Spinner from '../components/Spinner';
import { ScrollView } from 'react-native-gesture-handler';


const SignalsScreen = () => {
  
  const dispatch = useDispatch();
  const {patient_signals_list, is_loading_signals, loading_signals_error} = useSelector(state => state.signals);
  const {current_patient } = useSelector(state => state.patients);

  useEffect(() => {
    dispatch(getPatientSignals(current_patient.PatientID));    
  }, []);


  if (is_loading_signals) return (
      <SafeAreaView style={styles.centerStyle}>
        <Spinner style={styles.centerStyle} spinnerText='Loading Signals...'/>      
      </SafeAreaView>
  );
  
    if (loading_signals_error) return (
      <SafeAreaView style={styles.centerStyle}>
        <Text h3>{loading_signals_error}</Text>
        <Button
              title="Try Again"
              onPress={() => dispatch(getPatientSignals(current_patient.PatientID))}
          />          
      </SafeAreaView>
    );
  
  const signalsClick = () => {
    console.log(`---Event signals click Patient[${current_patient.PatientID}] `)
    dispatch(getPatientSignals(current_patient.PatientID))
  };

return (
  <ScrollView>
  <SafeAreaView>
    <Header headerText='Signal'/>
    <Spacer/>    
    <FlatList
          data={patient_signals_list}
          keyExtractor={() => createGUID()}
          renderItem={({item}) => (
            <TouchableOpacity  >
              <SignalListItem {...item} />
              </TouchableOpacity>
          )}       
       />  
      

              {/* <FlatList
              data={patient_signals_list}
              keyExtractor={() => createGUID()}
              renderItem={({item}) => {
                return <Text>{item.Abbreviation} {item.UpdateTime}</Text>
              }}
              /> */}
  </SafeAreaView>
  </ScrollView>  

  );
};


function createGUID() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
     var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
     return v.toString(16);
  });
}

const styles = StyleSheet.create({
  centerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  item: {
    color: 'yellow'
  }
});



export default SignalsScreen;
