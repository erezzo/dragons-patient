import React, { useEffect, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Input, Text, Button, CheckBox } from 'react-native-elements';
import { loadForm, submitForm, setControls, setTitle } from '../state/actions/form.actions';
import { useSelector, useDispatch } from "react-redux";
import { SafeAreaView } from 'react-native-safe-area-context';
import Header from '../components/Header';
import Spacer from '../components/Spacer';
import Spinner from '../components/Spinner';

const RegistrationFormScreen = ( { navigation } ) => {
 
  const dispatch = useDispatch();
  const { controls, title, is_loading, loading_error, is_submitting, submitting_error} = useSelector(state => state.form);
 
  const createControlsState = (data) => {
   
    if (!data || !data.controls) return;

		var newControls = [];
		data.controls.map((item, index) => {			         
			newControls.push({...item, value: "", index: index});		   
		});
    
    dispatch(setControls(newControls));
		
	};

  useEffect(() => {    
    dispatch(loadForm(createControlsState))
  }, []);

  if (is_loading) return (
    <SafeAreaView style={styles.centerStyle}>
      <Spinner style={styles.centerStyle} spinnerText='Loading Form...'/>      
    </SafeAreaView>
  );

  console.log(controls)
  if (loading_error || !controls) return (
    <SafeAreaView style={styles.centerStyle}>
      <Text h3>{loading_error}</Text>
      <Button
            title="Try Again"
            onPress={() => dispatch(loadForm(createControlsState))}
        />          
    </SafeAreaView>
  );


  const submitClick = () => {  
    const values = controls.map(item => ({"key": item.key, "value": item.value}))
    console.log(values)
    alert(values.map(({key, value}) => `Key=${key}, Value=${value}`).join('\n\r'))
    //dispatch(submitForm(formData))
  };

  const createControl = (item, index) => {
 
    switch (item.type) {
      case "freeText":
        return <Input     
          label={item.title}
          value={controls[index].value}
          key={index}
          onChangeText={(newText) => {
            const currentControl = {...controls[index], value: newText}
            dispatch(setControls([...controls.map(item => (item.key === controls[index].key ? currentControl : item))]))            
            }}     
        />;
      case "text":
        return <Input     
          label={item.title}
          value={controls[index].value}
          key={index}
          onChangeText={(newText) => {
            const currentControl = {...controls[index], value: newText}
            dispatch(setControls([...controls.map(item => (item.key === controls[index].key ? currentControl : item))]))            
            }}     
        />;        
        case "checkbox":
          return <CheckBox     
            title={item.title}
            checked={controls[index].value}
            //onChangeText={dispatch(setControls([...newControls]))}     
          />
    
      default:
    }
  }  

return (
  <SafeAreaView>
    <Header headerText={title || 'Patient Registration'}/>
    <Spacer/>
    
    {controls.map((item, index) => 
      createControl(item, index)
      )      
    }

    <Button
          title="Submit"
          onPress={submitClick}
        />
        
  </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  centerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default RegistrationFormScreen;
