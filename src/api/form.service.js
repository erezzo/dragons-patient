import formInfo from '../data/fake.form.json';
  
  const loadFormFake = () => {
    return new Promise((resolve, reject) => {
        setTimeout( function() {
            resolve(formInfo)
          }, 1500)
        })
  };
 
  const submitFormFake = (data) => {
    return new Promise((resolve, reject) => {
        setTimeout( function() {
            resolve(data)
          }, 1200)
        })
  };


  export default {
    loadFormFake,
    submitFormFake
  };