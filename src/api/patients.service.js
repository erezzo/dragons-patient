import axios from "axios";
//import { headerAuthorization, getBaseUrl } from "./auth.service";
import data from '../data/fake.patients.json';
//const Hawk = require('hawk');

//const AUTHORIZAION = `Hawk Auth ID="Rfj1ZcpLv9CI", Hawk Auth Key="Oup_W8a-Cl8vNS-dAb48gpv5T6gQ4GRqz3l-tFlhKqq", Algorithm="sha256", ext="user=c"`;
//const AUTHORIZAION = `ID="Rfj1ZcpLv9CI", Key="Oup_W8a-Cl8vNS-dAb48gpv5T6gQ4GRqz3l-tFlhKqq", Algorithm="sha256", ext="user=c"`;
const hawkAuth = {
  credentials: {
      id: "Rfj1ZcpLv9CI",
      key: "Oup_W8a-Cl8vNS-dAb48gpv5T6gQ4GRqz3l-tFlhKqq",
      algorithm: 'sha256',
  },
  ext: "user=c UserRole=TesterUserRole"
}

const headerAuthorization = () => {
  return { 'Authorization': AUTHORIZAION };
}

 const getPatients = () => {
   console.log(`----loading real data-----`);
   //const url = getBaseUrl();
   //console.log(`----BaseUrl=[${url}]-----`)
    //const path = `${url}API/V1/patients`;
    //const path = `http://aut-api-iis/MVAPI-CI-6.15/API/V1/patients`;
    const uri = `http://aut-api-iis/MVAPI-CI-6.15/API/V1/patients`;
    //const { header } = Hawk.client.header(uri, 'GET', { credentials: hawkAuth.credentials, ext: hawkAuth.ext });
    console.log(`----path=[${uri}]-----`)
    axios.interceptors.request.use(request => {
      console.log('Starting Request', JSON.stringify(request, null, 2))
      return request
    })
    axios.interceptors.response.use(response => {
      console.log('Response:', JSON.stringify(response, null, 2))
      return response
    })
    return axios.get(uri, { headers: { 'Authorization': hawkAuth } });
  };
  
  const getPatientsFake = () => {
    let patientResult = new Promise((resolve, reject) => {
        setTimeout( function() {
            resolve(data)
          }, 1500)
        })
    return patientResult;
  };
 
  const getPatient = (id) => {
    const path = `${BASE_API_URL}API/V1/patients/${id}`;
    return axios.get(path, { headers: headerAuthorization() });
  };


  export default {
    getPatientsFake,
    getPatient,
    getPatients
  };