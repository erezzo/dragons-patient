
import SignalsService from "../../api/signals.service";

export const GET_PATIENT_SIGNALS_START = "GET_PATIENT_SIGNALS_START";
export const GET_PATIENT_SIGNALS_SUCCESS = "GET_PATIENT_SIGNALS_SUCCESS";
export const GET_PATIENT_SIGNALS_FAIL = "GET_PATIENT_SIGNALS_FAIL";

const get_patient_signals_start = () => ({
    type: GET_PATIENT_SIGNALS_START
});
const get_patient_signals_success = (payload) => ({
    type: GET_PATIENT_SIGNALS_SUCCESS,
    payload
});
const get_patient_signals_fail = (payload) => ({
    type: GET_PATIENT_SIGNALS_FAIL,
    payload
});

  
export function getPatientSignals(patientId) {
  return async dispatch => {
    try {
      console.log(`----[GETPATIETNSIGNALS]Patient Id= ${patientId}`)

      console.log(`---------start get patient signals-------------`);
      dispatch(get_patient_signals_start()); // START
      const response = await SignalsService.getPatientSignals(patientId);
      console.log(`---------after get patient signals-------------`);
      console.log(response.data);
      dispatch(get_patient_signals_success(response.data)); // SUCCESS
    } catch (error) {
      console.log(`---------error get patient signals-------------`);
      dispatch(get_patient_signals_fail(`error: ${error} `)); // FAILED
    }
  };
};