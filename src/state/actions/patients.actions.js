import PatientsService from "../../api/patients.service";

export const GET_PATIENTS_START = "GET_PATIENTS_START";
export const GET_PATIENTS_SUCCESS = "GET_PATIENTS_SUCCESS";
export const GET_PATIENTS_FAIL = "GET_PATIENTS_FAIL";

export const GET_PATIENT_START = "GET_PATIENT_START";
export const GET_PATIENT_SUCCESS = "GET_PATIENT_SUCCESS";
export const GET_PATIENT_FAIL = "GET_PATIENT_FAIL";
export const SET_PATIENT = "SET_PATIENT";

const get_patients_start = () => ({
    type: GET_PATIENTS_START
});
const get_patients_success = (payload) => ({
    type: GET_PATIENTS_SUCCESS,
    payload
});
const get_patients_fail = (payload) => ({
    type: GET_PATIENTS_FAIL,
    payload
});

const get_patient_start = () => ({
    type: GET_PATIENT_START
});
const get_patient_success = (payload) => ({
    type: GET_PATIENT_SUCCESS,
    payload
});
const get_patient_fail = (payload) => ({
    type: GET_PATIENT_FAIL,
    payload
});

export const setPatient = (payload) => ({ 
  type: SET_PATIENT, 
  payload 
});

export function getPatients() {
    return async dispatch => {
      try {
        console.log(`---------start get patient-------------`);
        dispatch(get_patients_start()); // START
        const response = await PatientsService.getPatientsFake();
        console.log(`---------after get patient-------------`);
        dispatch(get_patients_success(response.data)); // SUCCESS
      } catch (error) {
        console.log(`---------error get patient-------------`);
        dispatch(get_patients_fail(`${error}`)); // FAILED
      }
    };
  };

  export function getPatient(id) {
    return async dispatch => {
      try {
        dispatch(get_patient_start()); // START
        const response = await PatientsService.getPatient();
        dispatch(get_patient_success(response.data)); // SUCCESS
      } catch (error) {
        dispatch(get_patient_fail(error.message)); // FAILED
      }
    };
  };