import FormService from "../../api/form.service";

export const LOAD_FORM_START = "LOAD_FORM_START";
export const LOAD_FORM_SUCCESS = "LOAD_FORM_SUCCESS";
export const LOAD_FORM_FAIL = "LOAD_FORM_FAIL ";
export const SUBMIT_FORM_START = "SUBMIT_FORM_START";
export const SUBMIT_FORM_SUCCESS = "SUBMIT_FORM_SUCCESS";
export const SUBMIT_FORM_FAIL = "SUBMIT_FORM_FAIL";
export const SET_CONTROLS = "SET_CONTROLS";
export const SET_TITLE = "SET_TITLE";

const load_form_start = () => ({
    type: LOAD_FORM_START
});
const load_form_success = (payload) => ({
    type: LOAD_FORM_SUCCESS,
    payload
});
const load_form_fail = (payload) => ({
    type: LOAD_FORM_FAIL,
    payload
});

const submit_form_start = () => ({
    type: SUBMIT_FORM_START
});
const submit_form_success = () => ({
    type: SUBMIT_FORM_SUCCESS
});
const submit_form_fail = (payload) => ({
    type: SUBMIT_FORM_FAIL,
    payload
});

export const setControls = (payload) => ({ 
    type: SET_CONTROLS, 
    payload 
  });
  
export const setTitle = (payload) => ({ 
    type: SET_TITLE, 
    payload 
  });

export function loadForm(callback) {
    return async dispatch => {
      try {
        console.log(`---------start load form-------------`);
        dispatch(load_form_start()); // START
        const response = await FormService.loadFormFake();
        console.log(`---------form template:-------------`);
        console.log(response);
        dispatch(load_form_success()); // SUCCESS
        if (callback)
            callback(response.data);
      } catch (error) {
        console.log(`---------error load form--------------`);
        dispatch(load_form_fail(`${error}`)); // FAILED
      }
    };
  };

  export function submitForm(data) {
    return async dispatch => {
      try {
        console.log(`---------start submit form-------------`);
        dispatch(submit_form_start()); // START
        const response = await FormService.submitFormFake();
        console.log(`---------submit form info: -------------`);  
        console.log(response);       
        dispatch(submit_form_success(response)); // SUCCESS
      } catch (error) {
        console.log(`---------error submit form--------------`);
        dispatch(submit_form_fail(`${error}`)); // FAILED
      }
    };
  };