export const BY_PATIENT = "BY_PATIENT";
export const IS_LOGGED_IN = "IS_LOGGED_IN";
export const BARCODE = "BARCODE ";

export const setByPatient = (payload) => ({ type: BY_PATIENT, payload });
export const setIsLogedIn = (payload) => ({ type: IS_LOGGED_IN, payload });
export const setBarcode = (barcodeType, data) => ({ type: BARCODE, barcodeType, data });