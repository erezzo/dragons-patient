import { LOAD_FORM_START, LOAD_FORM_SUCCESS, LOAD_FORM_FAIL,
    SUBMIT_FORM_START, SUBMIT_FORM_SUCCESS, SUBMIT_FORM_FAIL,
    SET_CONTROLS, SET_TITLE
} from "../actions/form.actions";

export const initialState = {
    controls: [],
    title: "",
    is_loading: false,
    loading_error: "",
    result: {},
    is_submitting: false,
    submitting_error: ""
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case LOAD_FORM_START:
        return {
          ...state,
          controls: [],
          title: "",
          is_loading: true,
          loading_error: ""
        };
      case LOAD_FORM_SUCCESS:
        return {
          ...state,
          is_loading: false          
        };
      case LOAD_FORM_FAIL:
        return {
          ...state,
          is_loading: false,
          loading_error: action.payload
        };
        case SUBMIT_FORM_START:
        return {
          ...state,
          result: {},
          is_submitting: true,
          submitting_error: ""
        };
      case SUBMIT_FORM_SUCCESS:
        return {
          ...state,
          is_submitting: false,
          result: action.payload
        };
      case SUBMIT_FORM_FAIL:
        return {
          ...state,
          is_submitting: false,
          submitting_error: action.payload
        };  
      case SET_CONTROLS:
        return {
            ...state,
            controls: action.payload
        };   
      case SET_TITLE:
        return {
            ...state,
            title: action.payload
        };        
      default:
        return state;
    }
  };
  export default reducer;