import { BY_PATIENT, IS_LOGGED_IN, BARCODE } from "../actions/general.actions";

export const initialState = {
    byPatient: true,
    isLoggedIn: false,
    barcodeType: "",
    barcodeData: ""
  };


const reducer = (state = initialState, action) => {
    switch (action.type) {
      case BY_PATIENT:
        return {
          ...state,
          byPatient: action.payload
        };
      case IS_LOGGED_IN:
        return {
            ...state,
            isLoggedIn: action.payload
          };
      case BARCODE:
        return {
          ...state,
          barcodeType: action.barcodeType,
          barcodeData: action.data
        };
      default:
        return state;
    }
  };
  export default reducer;