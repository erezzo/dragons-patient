import { combineReducers } from "redux";

import patientsReducer from "./patients.reducer";
import generalReducer from "./general.reducer";
import signalsReducer from "./signals.reducer";
import formReducer from "./form.reducer";

const rootReducer = combineReducers({
  patients: patientsReducer,
  signals: signalsReducer,
  general: generalReducer,
  form: formReducer
});

export default rootReducer;