import { GET_PATIENT_SIGNALS_START, GET_PATIENT_SIGNALS_SUCCESS, GET_PATIENT_SIGNALS_FAIL,
} from "../actions/signals.actions";

export const initialState = {
    patient_signals_list: [],
    is_loading_signals: false,
    loading_signals_error: ""
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case GET_PATIENT_SIGNALS_START:
        return {
          ...initialState, 
          is_loading_signals: true         
        };
      case GET_PATIENT_SIGNALS_SUCCESS:
        return {
          ...state,
          is_loading_signals: false,
          patient_signals_list: action.payload
        };
      case GET_PATIENT_SIGNALS_FAIL:
        return {
          ...state,
          is_loading_signals: false,
          loading_signals_error: action.payload
        };
      default:
        return state;
    }
  };
  export default reducer;