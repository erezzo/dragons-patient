import { GET_PATIENTS_START, GET_PATIENTS_SUCCESS, GET_PATIENTS_FAIL,
    GET_PATIENT_START, GET_PATIENT_SUCCESS, GET_PATIENT_FAIL,
    SET_PATIENT
} from "../actions/patients.actions";

export const initialState = {
    patients_list: [],
    is_loading_patients: false,
    loading_patients_error: "",
    current_patient: {},
    is_loading_patient: false,
    loading_patient_error: ""
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case GET_PATIENTS_START:
        return {
          ...state,
          patients_list: [],
          is_loading_patients: true,
          loading_patients_error: ""
        };
      case GET_PATIENTS_SUCCESS:
        return {
          ...state,
          is_loading_patients: false,
          patients_list: action.payload
        };
      case GET_PATIENTS_FAIL:
        return {
          ...state,
          is_loading_patients: false,
          loading_patients_error: action.payload
        };
        case GET_PATIENT_START:
        return {
          ...state,
          current_patient: {},
          is_loading_patient: true,
          loading_patient_error: ""
        };
      case GET_PATIENT_SUCCESS:
        return {
          ...state,
          is_loading_patient: false,
          current_patient: action.payload
        };
      case GET_PATIENT_FAIL:
        return {
          ...state,
          is_loading_patient: false,
          loading_patient_error: action.payload
        };
        case SET_PATIENT:
          return {
            ...state,
            current_patient: action.payload
          };
      default:
        return state;
    }
  };
  export default reducer;