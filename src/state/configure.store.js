import { createStore, applyMiddleware } from "redux";

import thunk from "redux-thunk";
import rootReducer from "./reducers/root.reducer";
import storage from "../middleware/localStorage";

const logger = (store) => (next) => (action) => {
  console.log("dispatching", action); 
  next(action);
  console.log("next state", store.getState());
};

const middleware = applyMiddleware(thunk);

const initialState = {
    patients: {
        patients_list: [],
        is_loading_patients: false,
        loading_patients_error: "",
        current_patient: {},
        is_loading_patient: false,
        loading_patient_error: ""
    },
    general: {
        byPatient: true,
        isLoggedIn: false
    }
};;

const store = createStore(
  rootReducer,
  initialState,
  middleware
);

store.subscribe(() => {
    storage.storeData(store.getState());
});

export default store;